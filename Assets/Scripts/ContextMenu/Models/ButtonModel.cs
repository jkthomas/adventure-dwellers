using System;

public class ButtonModel
{
  public string Text { get; }
  public BlockModel NestedContextMenuModel { get; } = null;
  public Action OnClickCallback { get; } = null;
  public bool CloseOnCallbackCalled = true;

  public ButtonModel(string text, BlockModel nestedContextMenuModel)
  {
    this.Text = text;
    this.NestedContextMenuModel = nestedContextMenuModel;
  }

  public ButtonModel(string text, Action onClickCallback, bool closeOnCallbackCalled = true)
  {
    this.Text = text;
    this.OnClickCallback = onClickCallback;
    this.CloseOnCallbackCalled = closeOnCallbackCalled;
  }
}