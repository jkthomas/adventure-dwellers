public class BlockModel
{
  public ButtonModel[] ButtonModels { get; }

  private float _buttonWidth;
  private float _buttonHeight;
  private float _buttonsVerticalPadding;
  private float _buttonsHorizontalPadding;

  public BlockModel(ButtonModel[] fields, float buttonWidth = 90, float buttonHeight = 15, float buttonsVerticalPadding = 3, float buttonsHorizontalPadding = 4)
  {
    this.ButtonModels = fields;
    this._buttonWidth = buttonWidth;
    this._buttonHeight = buttonHeight;
    this._buttonsVerticalPadding = buttonsVerticalPadding;
    this._buttonsHorizontalPadding = buttonsHorizontalPadding;
  }

  public float GetContextMenuWidth()
  {
    return this._buttonWidth + this._buttonsHorizontalPadding * 2;
  }

  public float GetContextMenuHeight()
  {
    /* 5 + 15 for paddings (including padding with top/botton) and height for each button, additional 5 to fill the gap between even number of blocks or missing padding between 2 middle button for odd number */
    return this.ButtonModels.Length * (this._buttonsVerticalPadding + this._buttonHeight) + this._buttonsVerticalPadding;
  }

  public float GetFirstButtonPositionOffsetY()
  {
    // even (2.5 from the middle of the block to the first button, 7.5 is to the middle of the target button, 5 is margin of each next button + 15 of each next button's height; Length / 2 - 1 because we take into consideration only top/bottom half and then we don't count the target button's height [because we want to put the middle of it in a specific position])
    // odd (7.5 from the middle of the block/middle_button to the edge of the button, then also 7.5 of the last button [we want to position it by the middle] so it adds to 15; for every button we take padding + height; then we add last padding manually because it is odd as a padding of the first two 7.5 mentioned)
    bool isButtonsLengthEven = this.ButtonModels.Length % 2 == 0;
    float positionOffsetY = isButtonsLengthEven ? ((this._buttonsVerticalPadding + this._buttonHeight) * (this.ButtonModels.Length / 2 - 1) + (this._buttonsVerticalPadding / 2 + this._buttonHeight / 2)) : ((this._buttonsVerticalPadding + this._buttonHeight) * ((this.ButtonModels.Length - 1) / 2));
    return positionOffsetY;
  }

  public float GetButtonPositionChangeOffsetY()
  {
    return this._buttonHeight + this._buttonsVerticalPadding;
  }
}
