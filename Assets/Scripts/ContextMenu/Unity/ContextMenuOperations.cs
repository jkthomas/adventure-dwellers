using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Add closing context menu segment by right-clicking on it
public class ContextMenuOperations : MonoBehaviour
{
  [SerializeField]
  private GameObject Button;
  private List<ContextMenuButtonOperations> ButtonsOperations = new List<ContextMenuButtonOperations>();

  public void InitialiseContextMenu(BlockModel contextMenuModel, bool isTopLevelContextMenu = false, Action closeContextMenuRoot = null)
  {
    RectTransform rt = this.gameObject.GetComponent<RectTransform>();
    rt.sizeDelta = new Vector2(contextMenuModel.GetContextMenuWidth(), contextMenuModel.GetContextMenuHeight());
    float positionOffsetY = contextMenuModel.GetFirstButtonPositionOffsetY();
    float positionChangeOffsetY = contextMenuModel.GetButtonPositionChangeOffsetY();
    foreach (ButtonModel buttonModel in contextMenuModel.ButtonModels)
    {
      Action closeContextMenuRootCallback = isTopLevelContextMenu ? () => { this.SetActiveStatus(false); }
      : closeContextMenuRoot;
      GameObject ButtonInstance = Instantiate(this.Button, new Vector3(this.transform.position.x, this.transform.position.y + positionOffsetY), Quaternion.identity, this.transform);
      ContextMenuButtonOperations buttonOperations = ButtonInstance.GetComponent<ContextMenuButtonOperations>();
      buttonOperations.InitialiseButton(
        buttonModel: buttonModel,
        disableSiblingsNestedContextMenus: this.DisableNestedButtons,
        closeContextMenuRoot: closeContextMenuRootCallback

      );
      this.ButtonsOperations.Add(buttonOperations);
      positionOffsetY -= positionChangeOffsetY;
    }
    if (isTopLevelContextMenu)
    {
      this.SetActiveStatus(false);
    }
  }

  public void UpdateLocation(Vector3 newPosition)
  {
    this.transform.position = newPosition;
  }

  private void DisableNestedButtons(ContextMenuButtonOperations buttonToOmit)
  {
    foreach (ContextMenuButtonOperations buttonOperations in this.ButtonsOperations)
    {
      if (buttonOperations != buttonToOmit && buttonOperations.HasActiveNestedContextMenu())
      {
        buttonOperations.DisableNestedContentMenu();
      }
    }
  }

  public void SetActiveStatus(bool newStatus)
  {
    if (newStatus != this.gameObject.activeSelf)
    {
      this.gameObject.SetActive(newStatus);
      foreach (ContextMenuButtonOperations buttonOperations in this.ButtonsOperations)
      {
        buttonOperations.ToggleActiveStatus();
      }
    }
  }

  public void SwitchActiveStatus()
  {
    if (this.gameObject.activeSelf)
    {
      this.SetActiveStatus(false);
    }
    else
    {
      this.SetActiveStatus(true);
    }
  }
}
