using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ContextMenuButtonOperations : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI Text;
  [SerializeField]
  private TextMeshProUGUI NestArrow;
  [SerializeField]
  private GameObject Block;
  private ContextMenuOperations NestedContextMenuOperations;
  private Action<ContextMenuButtonOperations> DisableSiblingsNestedContextMenus;

  public void InitialiseButton(ButtonModel buttonModel, Action<ContextMenuButtonOperations> disableSiblingsNestedContextMenus, Action closeContextMenuRoot)
  {
    this.Text.text = buttonModel.Text;
    EventTrigger trigger = this.GetComponent<EventTrigger>();
    EventTrigger.Entry onPointerClick = new EventTrigger.Entry();
    onPointerClick.eventID = EventTriggerType.PointerClick;

    if (buttonModel.NestedContextMenuModel != null)
    {
      this.NestArrow.text = ">";
      this.DisableSiblingsNestedContextMenus = disableSiblingsNestedContextMenus;

      onPointerClick.callback.AddListener((eventData) =>
      {
        if ((eventData as PointerEventData).button == PointerEventData.InputButton.Left)
        {
          this.OnClickWithNestedContextMenu();
        }
      });
      trigger.triggers.Add(onPointerClick);

      GameObject BlockInstance = Instantiate(this.Block, new Vector3(this.transform.position.x + buttonModel.NestedContextMenuModel.GetContextMenuWidth() + 5, this.transform.position.y), Quaternion.identity, this.transform);
      this.NestedContextMenuOperations = BlockInstance.GetComponent<ContextMenuOperations>();
      this.NestedContextMenuOperations.InitialiseContextMenu(buttonModel.NestedContextMenuModel, isTopLevelContextMenu: false, closeContextMenuRoot);
    }
    else
    {
      onPointerClick.callback.AddListener((eventData) =>
      {
        if ((eventData as PointerEventData).button == PointerEventData.InputButton.Left)
        {
          buttonModel.OnClickCallback();
          if (buttonModel.CloseOnCallbackCalled)
          {
            closeContextMenuRoot();
          }
        }
      });
      trigger.triggers.Add(onPointerClick);
    }
  }

  public bool HasActiveNestedContextMenu()
  {
    return this.NestedContextMenuOperations != null && this.NestedContextMenuOperations.gameObject.activeSelf;
  }

  public void ToggleActiveStatus()
  {
    this.gameObject.SetActive(!this.gameObject.activeSelf);
    if (this.gameObject.activeSelf == false && this.NestedContextMenuOperations != null)
    {
      this.NestedContextMenuOperations.SetActiveStatus(false);
    }
  }

  public void DisableNestedContentMenu()
  {
    if (this.NestedContextMenuOperations != null)
    {
      this.NestedContextMenuOperations.SetActiveStatus(false);
    }
  }

  private void OnClickWithNestedContextMenu()
  {
    if (this.NestedContextMenuOperations != null && this.DisableSiblingsNestedContextMenus != null)
    {
      this.DisableSiblingsNestedContextMenus(this);
      this.NestedContextMenuOperations.SetActiveStatus(!this.NestedContextMenuOperations.gameObject.activeSelf);
    }
  }
}
