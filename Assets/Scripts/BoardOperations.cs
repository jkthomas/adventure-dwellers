using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardOperations : MonoBehaviour
{
  [SerializeField]
  private GameObject Deck;

  void Start()
  {
    if (OverlayInstances.isInitialized && BoardTransforms.isInitialized)
    {
      Debug.Log("Shared static instances are initialized");
    }

    GameObject SpellsDeckInstance = Instantiate(this.Deck, new Vector2(50, 325), Quaternion.identity, BoardTransforms.PlayerBoard);
    SpellsDeckInstance.GetComponent<DeckOperations>().InitialiseDeck(CardType.Spell);
    SpellsDeckInstance.name = "SpellsDeck";

    GameObject FoesDeckInstance = Instantiate(this.Deck, new Vector2(50, 400), Quaternion.identity, BoardTransforms.PlayerBoard);
    FoesDeckInstance.GetComponent<DeckOperations>().InitialiseDeck(CardType.Foe);
    FoesDeckInstance.name = "FoesDeck";

    GameObject ItemsDeckInstance = Instantiate(this.Deck, new Vector2(50, 475), Quaternion.identity, BoardTransforms.PlayerBoard);
    ItemsDeckInstance.GetComponent<DeckOperations>().InitialiseDeck(CardType.Item);
    ItemsDeckInstance.name = "ItemsDeck";

    GameObject GraveyardInstance = Instantiate(this.Deck, new Vector2(50, 225), Quaternion.identity, BoardTransforms.PlayerBoard);
    GraveyardInstance.GetComponent<DeckOperations>().InitialiseDeck(CardType.Any);
    GraveyardInstance.name = "Graveyard";
  }
}

// TODO:
// Big counters for card inspector

// Low-priority TODOs:
// Hide all context menus button? Click anywhere on board to close all context menus?
// Change crystals switching from clicking to context menu + save