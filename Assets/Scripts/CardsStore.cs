using System;
using System.Collections;
using System.Collections.Generic;

public static class CardsStore
{
  private static List<CardData> spellCards = new List<CardData> {
      new CardData("Spell1", "Description1", CardType.Spell),
      new CardData("Spell2", "Description2", CardType.Spell),
      new CardData("Spell3", "Description3", CardType.Spell),
      new CardData("Spell4", "Description4", CardType.Spell),
      new CardData("Spell5", "Description5", CardType.Spell),
      new CardData("Spell6", "Description6", CardType.Spell),
      new CardData("Spell7", "Description7", CardType.Spell),
      new CardData("Spell8", "Description8", CardType.Spell),
      new CardData("Spell9", "Description9", CardType.Spell),
      new CardData("Spell10", "Description10", CardType.Spell),
      new CardData("Spell11", "Description11", CardType.Spell),
      new CardData("Spell12", "Description12", CardType.Spell),
    };

  private static List<CardData> foeCards = new List<CardData> {
      new CardData("Foe1", "Description1", CardType.Foe),
      new CardData("Foe2", "Description2", CardType.Foe),
      new CardData("Foe3", "Description3", CardType.Foe),
      new CardData("Foe4", "Description4", CardType.Foe),
      new CardData("Foe5", "Description5", CardType.Foe),
      new CardData("Foe6", "Description6", CardType.Foe),
    };

  private static List<CardData> itemCards = new List<CardData> {
      new CardData("Item1", "Description1", CardType.Item),
      new CardData("Item2", "Description2", CardType.Item),
      new CardData("Item3", "Description3", CardType.Item),
      new CardData("Item4", "Description4", CardType.Item),
      new CardData("Item5", "Description5", CardType.Item),
      new CardData("Item6", "Description6", CardType.Item),
      new CardData("Item7", "Description7", CardType.Item),
      new CardData("Item8", "Description8", CardType.Item),
      new CardData("Item9", "Description9", CardType.Item),
      new CardData("Item10", "Description10", CardType.Item),
      new CardData("Item11", "Description11", CardType.Item),
      new CardData("Item12", "Description12", CardType.Item),
      new CardData("Item13", "Description13", CardType.Item),
      new CardData("Item14", "Description14", CardType.Item),
      new CardData("Item15", "Description15", CardType.Item),
      new CardData("Item16", "Description16", CardType.Item),
      new CardData("Item17", "Description17", CardType.Item),
      new CardData("Item18", "Description18", CardType.Item),
      new CardData("Item19", "Description19", CardType.Item),
    };

  private static List<CardData> emptyDeck = new List<CardData> { };
  public static List<CardData> GetCards(CardType cardType) => cardType switch
  {
    CardType.Spell => spellCards,
    CardType.Foe => foeCards,
    CardType.Item => itemCards,
    CardType.Any => emptyDeck,
    _ => throw new ArgumentOutOfRangeException(nameof(cardType), $"Not expected direction value: {cardType}"),
  };
}