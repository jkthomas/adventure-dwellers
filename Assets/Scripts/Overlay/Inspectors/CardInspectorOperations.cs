using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardInspectorOperations : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI Title;
  [SerializeField]
  private TextMeshProUGUI Description;

  [SerializeField]
  private GameObject RedCounter;
  [SerializeField]
  private TextMeshProUGUI RedCounterText;

  [SerializeField]
  private GameObject GreenCounter;
  [SerializeField]
  private TextMeshProUGUI GreenCounterText;

  [SerializeField]
  private GameObject BlueCounter;
  [SerializeField]
  private TextMeshProUGUI BlueCounterText;

  [SerializeField]
  private GameObject BlackCounter;
  [SerializeField]
  private TextMeshProUGUI BlackCounterText;

  public void InspectCard(CardData card)
  {
    this.Title.text = card.Title;
    this.Description.text = card.Description;
    if (!this.gameObject.activeSelf)
    {
      this.gameObject.SetActive(true);
    }

    // TODO: Refactor, so it won't have so much reduntant code
    if (card.RedCounters != 0)
    {
      this.RedCounterText.text = card.RedCounters.ToString();
      this.RedCounter.SetActive(true);
    }
    else
    {
      this.RedCounter.SetActive(false);
    }
    if (card.GreenCounters != 0)
    {
      this.GreenCounterText.text = card.GreenCounters.ToString();
      this.GreenCounter.SetActive(true);
    }
    else
    {
      this.GreenCounter.SetActive(false);
    }
    if (card.BlueCounters != 0)
    {
      this.BlueCounterText.text = card.BlueCounters.ToString();
      this.BlueCounter.SetActive(true);
    }
    else
    {
      this.BlueCounter.SetActive(false);
    }
    if (card.BlackCounters != 0)
    {
      this.BlackCounterText.text = card.BlackCounters.ToString();
      this.BlackCounter.SetActive(true);
    }
    else
    {
      this.BlackCounter.SetActive(false);
    }
  }

  public void OnExitButtonClicked()
  {
    this.gameObject.SetActive(false);
  }
}
