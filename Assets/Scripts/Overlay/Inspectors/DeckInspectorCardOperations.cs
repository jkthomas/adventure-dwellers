using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class DeckInspectorCardOperations : MonoBehaviour, IPointerClickHandler
{
  [SerializeField]
  private TextMeshProUGUI Title;
  [SerializeField]
  private TextMeshProUGUI Description;
  [SerializeField]
  private GameObject ContextMenu;
  private CardData cardData;
  private ContextMenuOperations ContextMenuOperations;

  public void SetCardProperties(CardData cardData)
  {
    this.cardData = cardData;
    this.Title.text = this.cardData.Title;
    this.Description.text = this.cardData.Description;

    BlockModel contextMenuModel = new BlockModel(
      new ButtonModel[1] {
            new ButtonModel(text: "Inspect", onClickCallback: () => { OverlayInstances.CardInspectorOperations.InspectCard(this.cardData); }),
      }
    );
    GameObject ContextMenuInstance = Instantiate(this.ContextMenu, this.transform);
    this.ContextMenuOperations = ContextMenuInstance.GetComponent<ContextMenuOperations>();
    this.ContextMenuOperations.InitialiseContextMenu(contextMenuModel, true);
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    if (eventData.button == PointerEventData.InputButton.Right)
    {
      this.ContextMenuOperations.UpdateLocation(this.transform.position);
      this.ContextMenuOperations.SwitchActiveStatus();
    }
  }

  public bool IsCardEqual(CardData cardData)
  {
    return Object.Equals(this.cardData, cardData);
  }
}
