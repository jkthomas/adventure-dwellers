using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

public class DeckInspectorOperations : MonoBehaviour
{
  // TODO: Refactor the usage - logic seems convoluted between Deck and Inspector operations
  // TODO: Handle shuffle in this operations
  // TODO: Refactor the inspector with Transforms handling and instances holding
  [SerializeField]
  private GameObject DeckBox;
  [SerializeField]
  private GameObject Card;
  private ObservableCollection<GameObject> CardsInstances = new ObservableCollection<GameObject>();
  private ObservableCollection<CardData> cards = null;

  // Start is called before the first frame update
  void Start()
  {
    for (int i = 0; i < 30; i++)
    {
      this.CardsInstances.Add(Instantiate(this.Card, this.DeckBox.transform));
    }
  }

  private void CloseInspector()
  {
    this.cards.CollectionChanged -= HandleDeckChanged;
    this.gameObject.SetActive(false);
  }

  private void HandleDeckChanged(object sender, NotifyCollectionChangedEventArgs e)
  {
    if (e.NewStartingIndex != -1 && e.OldStartingIndex != -1)
    {
      // Both changed, meaning a Shuffle -> Exit the inspector
      this.CloseInspector();
      return;
    }
    else if (e.NewStartingIndex != -1)
    {
      CardData addedCardData = e.NewItems[0] as CardData;
      foreach (Transform cardTransform in this.DeckBox.GetComponentInChildren<Transform>())
      {
        if (!cardTransform.gameObject.activeSelf)
        {
          cardTransform.gameObject.GetComponent<DeckInspectorCardOperations>().SetCardProperties(addedCardData);
          cardTransform.SetAsFirstSibling();
          cardTransform.gameObject.SetActive(true);
          break;
        }
      }
    }
    else if (e.OldStartingIndex != -1)
    {
      CardData removedCardData = e.OldItems[0] as CardData;
      foreach (Transform cardTransform in this.DeckBox.GetComponentInChildren<Transform>())
      {
        if (cardTransform.gameObject.GetComponent<DeckInspectorCardOperations>().IsCardEqual(removedCardData))
        {
          if (cardTransform.gameObject.activeSelf)
          {
            cardTransform.gameObject.SetActive(false);
            cardTransform.SetAsLastSibling();
            break;
          }
          else
          {
            // Already turned off
            break;
          }
        }
      }
    }
  }

  public void ShowDeck(ObservableCollection<CardData> cards)
  {
    cards.CollectionChanged -= HandleDeckChanged;
    cards.CollectionChanged += HandleDeckChanged;

    if (!this.gameObject.activeSelf)
    {
      this.gameObject.SetActive(true);
    }
    for (int i = 0; i < 30; i++)
    {
      if (i < cards.Count)
      {
        this.CardsInstances[i].GetComponent<DeckInspectorCardOperations>().SetCardProperties(cards[i]);
        this.CardsInstances[i].transform.SetAsLastSibling();
        this.CardsInstances[i].gameObject.SetActive(true);
      }
      else
      {
        this.CardsInstances[i].gameObject.SetActive(false);
        this.CardsInstances[i].transform.SetAsLastSibling();
      }
    }
    this.cards = cards;
  }

  public void OnExitButtonClicked()
  {
    this.CloseInspector();
  }
}
