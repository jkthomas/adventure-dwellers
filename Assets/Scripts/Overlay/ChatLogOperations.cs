using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChatLogOperations : MonoBehaviour
{
  // TODO: Add multiline entries - for now input is limited to 35 characters to avoid breaking
  [SerializeField]
  private GameObject ChatboxContent;
  [SerializeField]
  private GameObject ChatEntry;
  [SerializeField]
  private GameObject ChatInputField;

  void Start()
  {
    GameObject ChatEntry = Instantiate(this.ChatEntry, this.ChatboxContent.transform);
    ChatEntry.GetComponent<TextMeshProUGUI>().text = "CHAT LOG START";
  }

  public void AddChatLogEntry(string log)
  {
    GameObject ChatEntry = Instantiate(this.ChatEntry, this.ChatboxContent.transform);
    ChatEntry.GetComponent<TextMeshProUGUI>().text = $"[{DateTime.Now.ToString("HH:mm:ss")}] {log}";
  }

  public void OnSubmit()
  {
    TMP_InputField inputField = this.ChatInputField.GetComponent<TMP_InputField>();

    GameObject ChatEntry = Instantiate(this.ChatEntry, this.ChatboxContent.transform);
    ChatEntry.GetComponent<TextMeshProUGUI>().text = $"[{DateTime.Now.ToString("HH:mm:ss")}] Player: {inputField.text}";

    inputField.text = "";
  }

  public void OnExitButtonClicked()
  {
    this.gameObject.SetActive(false);
  }

  public void SwitchActiveStatus()
  {
    if (this.gameObject.activeSelf)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
    }
  }
}
