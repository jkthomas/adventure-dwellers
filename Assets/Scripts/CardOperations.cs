using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class CardOperations : MonoBehaviour, IPointerClickHandler
{
  private Vector3 _originalPosition;
  private Transform _originalParent;
  public LayerMask DecksLayerMask;
  public LayerMask BoardSectorsLayerMask;

  [SerializeField]
  private TextMeshProUGUI Title;
  [SerializeField]
  private TextMeshProUGUI Description;
  [SerializeField]
  private GameObject ContextMenu;

  [SerializeField]
  private GameObject RedCounter;
  [SerializeField]
  private TextMeshProUGUI RedCounterText;

  [SerializeField]
  private GameObject GreenCounter;
  [SerializeField]
  private TextMeshProUGUI GreenCounterText;

  [SerializeField]
  private GameObject BlueCounter;
  [SerializeField]
  private TextMeshProUGUI BlueCounterText;

  [SerializeField]
  private GameObject BlackCounter;
  [SerializeField]
  private TextMeshProUGUI BlackCounterText;

  private bool _isDragged = false;
  private CardData cardData;
  private string deckName;
  private ContextMenuOperations ContextMenuOperations;

  public void SetCardProperties(CardData cardData, string deckName)
  {
    this.cardData = cardData;
    this.Title.text = this.cardData.Title;
    this.Description.text = this.cardData.Description;
    this.deckName = deckName;
    // TODO: Add enum for counter colors
    BlockModel contextMenuModel = new BlockModel(
      new ButtonModel[3] {
        new ButtonModel(text: "+ counter", nestedContextMenuModel: new BlockModel(
          fields: new ButtonModel[4] {
            new ButtonModel(text: "Red", onClickCallback: () => { this.IncrementCounter("Red"); }),
            new ButtonModel(text: "Green", onClickCallback: () => { this.IncrementCounter("Green"); }),
            new ButtonModel(text: "Blue", onClickCallback: () => { this.IncrementCounter("Blue"); }),
            new ButtonModel(text: "Black", onClickCallback: () => { this.IncrementCounter("Black"); }),
          }
        )),
        new ButtonModel(text: "- counter", nestedContextMenuModel: new BlockModel(
          fields: new ButtonModel[4] {
            new ButtonModel(text: "Red", onClickCallback: () => { this.DecrementCounter("Red"); }),
            new ButtonModel(text: "Green", onClickCallback: () => { this.DecrementCounter("Green"); }),
            new ButtonModel(text: "Blue", onClickCallback: () => { this.DecrementCounter("Blue"); }),
            new ButtonModel(text: "Black", onClickCallback: () => { this.DecrementCounter("Black"); }),
          }
        )),
        new ButtonModel(text: "Inspect", onClickCallback: () => { OverlayInstances.CardInspectorOperations.InspectCard(this.cardData); }),
      }
    );
    GameObject ContextMenuInstance = Instantiate(this.ContextMenu, this.transform);
    this.ContextMenuOperations = ContextMenuInstance.GetComponent<ContextMenuOperations>();
    this.ContextMenuOperations.InitialiseContextMenu(contextMenuModel, true);
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    if (!this._isDragged)
    {
      if (eventData.button == PointerEventData.InputButton.Right)
      {
        this.ContextMenuOperations.UpdateLocation(this.transform.position);
        this.ContextMenuOperations.SwitchActiveStatus();
      }
    }
  }

  public void StartDragging()
  {
    this._originalPosition = this.transform.position;
    this._originalParent = this.transform.parent;
    this._isDragged = true;
    this.transform.SetParent(BoardTransforms.Board, true);
    this.ContextMenuOperations.SetActiveStatus(false);
  }

  public void StopDragging()
  {
    _isDragged = false;

    Collider2D deckLayerCollider = Physics2D.OverlapBox(this.transform.position, Vector2.zero, 0, DecksLayerMask);
    if (deckLayerCollider != null)
    {
      bool wasCardAddedToDeck = deckLayerCollider.gameObject.GetComponent<DeckOperations>().AddTopCard(this.cardData);
      if (wasCardAddedToDeck)
      {
        Destroy(this.gameObject);
        return;
      }
    }
    else
    {
      Collider2D boardSectionsLayerCollider = Physics2D.OverlapBox(this.transform.position, Vector2.zero, 0, BoardSectorsLayerMask);
      if (boardSectionsLayerCollider != null)
      {
        if (boardSectionsLayerCollider.transform.childCount < BoardTransforms.GetMaxAllowedCardsNumber(boardSectionsLayerCollider.transform))
        {
          this.transform.SetParent(boardSectionsLayerCollider.transform, true);
          OverlayInstances.ChatLogOperations.AddChatLogEntry($"Card {this.cardData.Title} moved from {this._originalParent.name} to {boardSectionsLayerCollider.transform.name}");
          return;
        }
        else
        {
          OverlayInstances.ChatLogOperations.AddChatLogEntry($"{boardSectionsLayerCollider.gameObject.name} has max number of cards on it");
        }
      }
    }

    Debug.Log("Reverting the card position...");
    this.transform.position = this._originalPosition;
    this.transform.SetParent(this._originalParent);
  }

  // TODO: Refactor the functions, the code is redundant for each color case
  // TODO: Add chat log entry after refactor
  private void IncrementCounter(string counterColor)
  {
    switch (counterColor)
    {
      case "Red":
        {
          this.RedCounterText.text = (Convert.ToInt32(this.RedCounterText.text) + 1).ToString();
          this.cardData.RedCounters = Convert.ToInt32(this.RedCounterText.text);
          if (!this.RedCounter.activeSelf)
          {
            this.RedCounter.SetActive(true);
          }
          break;
        }
      case "Green":
        {
          this.GreenCounterText.text = (Convert.ToInt32(this.GreenCounterText.text) + 1).ToString();
          this.cardData.GreenCounters = Convert.ToInt32(this.GreenCounterText.text);
          if (!this.GreenCounter.activeSelf)
          {
            this.GreenCounter.SetActive(true);
          }
          break;
        }
      case "Blue":
        {
          this.BlueCounterText.text = (Convert.ToInt32(this.BlueCounterText.text) + 1).ToString();
          this.cardData.BlueCounters = Convert.ToInt32(this.BlueCounterText.text);
          if (!this.BlueCounter.activeSelf)
          {
            this.BlueCounter.SetActive(true);
          }
          break;
        }
      case "Black":
        {
          this.BlackCounterText.text = (Convert.ToInt32(this.BlackCounterText.text) + 1).ToString();
          this.cardData.BlackCounters = Convert.ToInt32(this.BlackCounterText.text);
          if (!this.BlackCounter.activeSelf)
          {
            this.BlackCounter.SetActive(true);
          }
          break;
        }
      default:
        Debug.Log("Unsupported counter color provided...");
        break;
    }
  }

  // TODO: Refactor the functions, the code is redundant for each color case
  // TODO: Add chat log entry after refactor
  private void DecrementCounter(string counterColor)
  {
    switch (counterColor)
    {
      case "Red":
        {
          if (this.RedCounter.activeSelf)
          {
            int counterValue = Convert.ToInt32(this.RedCounterText.text);
            this.RedCounterText.text = (counterValue - 1).ToString();
            this.cardData.RedCounters = Convert.ToInt32(this.RedCounterText.text);
            if (this.cardData.RedCounters == 0)
            {
              this.RedCounter.SetActive(false);
            }
          }
          break;
        }
      case "Green":
        {
          if (this.GreenCounter.activeSelf)
          {
            int counterValue = Convert.ToInt32(this.GreenCounterText.text);
            this.GreenCounterText.text = (counterValue - 1).ToString();
            this.cardData.GreenCounters = Convert.ToInt32(this.GreenCounterText.text);
            if (this.cardData.GreenCounters == 1)
            {
              this.GreenCounter.SetActive(false);
            }
          }
          break;
        }
      case "Blue":
        {
          if (this.BlueCounter.activeSelf)
          {
            int counterValue = Convert.ToInt32(this.BlueCounterText.text);
            this.BlueCounterText.text = (counterValue - 1).ToString();
            this.cardData.BlueCounters = Convert.ToInt32(this.BlueCounterText.text);
            if (this.cardData.BlueCounters == 0)
            {
              this.BlueCounter.SetActive(false);
            }
          }
          break;
        }
      case "Black":
        {
          if (this.BlackCounter.activeSelf)
          {
            int counterValue = Convert.ToInt32(this.BlackCounterText.text);
            this.BlackCounterText.text = (counterValue - 1).ToString();
            this.cardData.BlackCounters = Convert.ToInt32(this.BlackCounterText.text);
            if (this.cardData.BlackCounters == 0)
            {
              this.BlackCounter.SetActive(false);
            }
          }
          break;
        }
      default:
        Debug.Log("Unsupported counter color provided...");
        break;
    }

  }

  void Update()
  {
    if (this._isDragged)
    {
      this.transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }
  }
}
