using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System.Collections.ObjectModel;

public class DeckOperations : MonoBehaviour, IPointerClickHandler
{
  [SerializeField]
  private GameObject Card;
  [SerializeField]
  private GameObject ContextMenu;
  [SerializeField]
  private TextMeshProUGUI CardsCount;
  private ObservableCollection<CardData> _cardsList;
  private CardType _deckType = CardType.Unspecified;
  private ContextMenuOperations ContextMenuOperations;

  public void InitialiseDeck(CardType deckType)
  {
    this._deckType = deckType;
    this._cardsList = new ObservableCollection<CardData>(CardsStore.GetCards(deckType));
    this.transform.Find("Type").GetComponent<TMP_Text>().text = deckType.ToString();
    this.CardsCount.text = this._cardsList.Count.ToString();
  }

  public void DrawCard(Transform targetBoardSectionTransform)
  {
    if (this._cardsList.Count <= 0)
    {
      OverlayInstances.ChatLogOperations.AddChatLogEntry($"No more cards to take from the {this.name} deck");
      return;
    }

    CardData card = this._cardsList[0];
    if (targetBoardSectionTransform.childCount >= BoardTransforms.GetMaxAllowedCardsNumber(targetBoardSectionTransform))
    {
      OverlayInstances.ChatLogOperations.AddChatLogEntry($"Too many cards in {targetBoardSectionTransform.name} deck");
      return;
    }
    GameObject CardInstance = Instantiate(this.Card, targetBoardSectionTransform);
    CardInstance.GetComponent<CardOperations>().SetCardProperties(
      card,
      this.gameObject.name
    );
    CardInstance.name = card.Title;
    this._cardsList.RemoveAt(0);
    this.CardsCount.text = this._cardsList.Count.ToString();

    OverlayInstances.ChatLogOperations.AddChatLogEntry($"Card {card.Title} drawn from {this.gameObject.name} deck to {targetBoardSectionTransform.name}");
  }

  private void ShuffleDeck()
  {
    /* Fisher-Yates shuffle algorithm */
    System.Random rng = new System.Random();
    int n = this._cardsList.Count;
    while (n > 1)
    {
      n--;
      int k = rng.Next(n + 1);
      CardData card = this._cardsList[k];
      this._cardsList[k] = this._cardsList[n];
      this._cardsList[n] = card;
    }

    OverlayInstances.ChatLogOperations.AddChatLogEntry($"Deck {this.gameObject.name} has been shuffled");
  }

  public bool AddTopCard(CardData cardData)
  {
    if (this._deckType == CardType.Any || cardData.Type == this._deckType)
    {
      this._cardsList.Insert(0, cardData);
      this.CardsCount.text = this._cardsList.Count.ToString();
      OverlayInstances.ChatLogOperations.AddChatLogEntry($"Card {cardData.Title} added to {this.gameObject.name} deck");
      return true;
    }
    else
    {
      OverlayInstances.ChatLogOperations.AddChatLogEntry($"Cannot add card to the deck: Incorrect card type {cardData.Type.ToString()} for this deck of type {this._deckType.ToString()}");
    }
    return false;
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    if (eventData.button == PointerEventData.InputButton.Right)
    {
      this.ContextMenuOperations.SwitchActiveStatus();
    }
  }

  private BlockModel GetContextMenuByDeckType(CardType deckType)
  {
    if (deckType == CardType.Unspecified)
    {
      return new BlockModel(new ButtonModel[0] { });
    }
    else if (deckType == CardType.Foe)
    {
      return new BlockModel(
        new ButtonModel[3] {
          new ButtonModel(text: "Draw", onClickCallback: () => { this.DrawCard(BoardTransforms.FoeBoard); }),
          new ButtonModel(text: "Shuffle", onClickCallback: () => { this.ShuffleDeck(); }),
          new ButtonModel(text: "Inspect", onClickCallback: () => { OverlayInstances.DeckInspectorOperations.ShowDeck(this._cardsList); }),
        }
      );
    }
    else
    {
      return new BlockModel(new ButtonModel[3] {
        new ButtonModel(text: "Draw", nestedContextMenuModel: new BlockModel(
          fields: new ButtonModel[4] {
            new ButtonModel(text: "Player 1", onClickCallback: () => { this.DrawCard(BoardTransforms.PlayerHand1); }, closeOnCallbackCalled: false),
            new ButtonModel(text: "Player 2", onClickCallback: () => { this.DrawCard(BoardTransforms.PlayerHand2); }, closeOnCallbackCalled: false),
            new ButtonModel(text: "Player 3", onClickCallback: () => { this.DrawCard(BoardTransforms.PlayerHand3); }, closeOnCallbackCalled: false),
            new ButtonModel(text: "Player 4", onClickCallback: () => { this.DrawCard(BoardTransforms.PlayerHand4); }, closeOnCallbackCalled: false),
          }
        )),
        new ButtonModel(text: "Shuffle", onClickCallback: () => { this.ShuffleDeck(); }),
        new ButtonModel(text: "Inspect", onClickCallback: () => { OverlayInstances.DeckInspectorOperations.ShowDeck(this._cardsList); }),
      });
    }
  }

  void Start()
  {
    BlockModel topLevelContextMenuModel = this.GetContextMenuByDeckType(this._deckType);
    GameObject ContextMenuInstance = Instantiate(this.ContextMenu, new Vector2(this.transform.position.x, this.transform.position.y), Quaternion.identity, this.transform);
    this.ContextMenuOperations = ContextMenuInstance.GetComponent<ContextMenuOperations>();
    this.ContextMenuOperations.InitialiseContextMenu(topLevelContextMenuModel, true);
  }
}
