using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalOperations : MonoBehaviour
{
  private class CrystalColor
  {
    public string name { get; }
    public Color color { get; }

    public CrystalColor(string name, Color color)
    {
      this.name = name;
      this.color = color;
    }
  }

  private int _currentColorIndex = 0;
  private CrystalColor[] _colors = {
    new CrystalColor("none", new Color32(255, 255, 255, 255)),
    new CrystalColor("Red",new Color32(255, 0, 0, 255)),
    new CrystalColor("Green",new Color32(0, 200, 0, 255)),
    new CrystalColor("Blue",new Color32(0, 100, 255, 255)),
  };

  public void OnCrystalClicked()
  {
    if (this._currentColorIndex == (this._colors.Length - 1))
    {
      this._currentColorIndex = 0;
    }
    else
    {
      this._currentColorIndex += 1;
    }
    this.UpdateCrystalColor();
    OverlayInstances.ChatLogOperations.AddChatLogEntry($"Crystal color changed to {this._colors[this._currentColorIndex].name}");
  }

  private void UpdateCrystalColor()
  {
    this.gameObject.GetComponent<Image>().color = this._colors[this._currentColorIndex].color;
  }

  void Start()
  {
    this.UpdateCrystalColor();
  }
}
