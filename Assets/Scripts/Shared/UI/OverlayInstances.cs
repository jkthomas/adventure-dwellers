using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class OverlayInstances
{
  public static readonly DeckInspectorOperations DeckInspectorOperations;
  public static readonly CardInspectorOperations CardInspectorOperations;
  public static readonly ChatLogOperations ChatLogOperations;

  public static readonly bool isInitialized = true;

  static OverlayInstances()
  {
    DeckInspectorOperations = GameObject.Find("DeckInspector").GetComponent<DeckInspectorOperations>();
    DeckInspectorOperations.gameObject.SetActive(false);

    CardInspectorOperations = GameObject.Find("CardInspector").GetComponent<CardInspectorOperations>();
    CardInspectorOperations.gameObject.SetActive(false);

    ChatLogOperations = GameObject.Find("ChatLog").GetComponent<ChatLogOperations>();
    ChatLogOperations.gameObject.SetActive(false);


    GameObject.Find("ChatLogButton").GetComponent<Button>().onClick.AddListener(() =>
    {
      ChatLogOperations.SwitchActiveStatus();
    });
    GameObject.Find("DiceRollButton").GetComponent<Button>().onClick.AddListener(() =>
    {
      int number = Random.Range(1, 6);
      ChatLogOperations.AddChatLogEntry($"Rolled {number} on a d6 dice");
    });
  }
}
