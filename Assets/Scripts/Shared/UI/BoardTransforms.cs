using System;
using UnityEngine;

public static class BoardTransforms
{
  public static readonly Transform Board;
  public static readonly Transform PlayerBoard;
  public static readonly Transform PlayerHand1;
  public static readonly Transform PlayerHand2;
  public static readonly Transform PlayerHand3;
  public static readonly Transform PlayerHand4;
  public static readonly Transform FoeBoard;
  public static readonly Transform FoeSideboardBuffs;
  public static readonly Transform FoeSideboardDebuffs;

  public static readonly bool isInitialized = true;

  static BoardTransforms()
  {
    Board = GameObject.Find("Board").transform;
    PlayerBoard = GameObject.Find("PlayerBoard").transform;
    PlayerHand1 = GameObject.Find("PlayerHand1").transform;
    PlayerHand2 = GameObject.Find("PlayerHand2").transform;
    PlayerHand3 = GameObject.Find("PlayerHand3").transform;
    PlayerHand4 = GameObject.Find("PlayerHand4").transform;
    FoeBoard = GameObject.Find("FoeBoard").transform;
    FoeSideboardBuffs = GameObject.Find("FoeSideboardBuffs").transform;
    FoeSideboardDebuffs = GameObject.Find("FoeSideboardDebuffs").transform;
  }

  public static Transform GetPlayerHandTransform(int targetPlayerNumber) => targetPlayerNumber switch
  {
    1 => PlayerHand1,
    2 => PlayerHand2,
    3 => PlayerHand3,
    4 => PlayerHand4,
    _ => throw new ArgumentOutOfRangeException(nameof(targetPlayerNumber), $"Not expected player number: {targetPlayerNumber}"),
  };

  public static int GetMaxAllowedCardsNumber(Transform targetTransform)
  {
    if (targetTransform == PlayerBoard)
    {
      return 100;
    }
    if (targetTransform == PlayerHand1)
    {
      return 4;
    }
    if (targetTransform == PlayerHand2)
    {
      return 4;
    }
    if (targetTransform == PlayerHand3)
    {
      return 4;
    }
    if (targetTransform == PlayerHand4)
    {
      return 4;
    }
    if (targetTransform == FoeBoard)
    {
      return 1;
    }
    if (targetTransform == FoeSideboardBuffs)
    {
      return 7;
    }
    if (targetTransform == FoeSideboardDebuffs)
    {
      return 7;
    }
    return 1;
  }
}
