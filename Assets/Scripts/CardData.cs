public class CardData
{
  public string Title { get; set; }
  public string Description { get; set; }
  public CardType Type { get; set; }
  public int RedCounters { get; set; }
  public int GreenCounters { get; set; }
  public int BlueCounters { get; set; }
  public int BlackCounters { get; set; }


  public CardData(string title, string description, CardType type, int redCounters = 0, int greenCounters = 0, int blueCounters = 0, int blackCounters = 0)
  {
    Title = title;
    Description = description;
    Type = type;
    RedCounters = redCounters;
    GreenCounters = greenCounters;
    BlueCounters = blueCounters;
    BlackCounters = blackCounters;
  }
}
