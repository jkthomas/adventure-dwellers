public enum CardType
{
  Unspecified,
  Any,
  Spell,
  Foe,
  Item
}
